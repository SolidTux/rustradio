#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
import matplotlib.pyplot as plt

samp_rate = 100e3

data = np.fromfile('test.iq', np.csingle)
t = np.arange(0, len(data))/samp_rate
# data = 10.*np.log10(np.abs(data))
data = np.abs(data)
plt.plot(t, data)
plt.tight_layout()
plt.show()
