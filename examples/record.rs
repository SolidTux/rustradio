use byteorder::LittleEndian;
use rustradio::prelude::*;
use soapysdr::{Device, Direction};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    let mut p = Program::default();

    let d = Device::new("")?;
    d.set_antenna(Direction::Rx, 0, "TX/RX")?;
    d.set_bandwidth(Direction::Rx, 0, 20e6)?;
    d.set_sample_rate(Direction::Rx, 0, 20e6)?;
    d.set_gain(Direction::Rx, 0, 10.)?;
    d.set_frequency(Direction::Rx, 0, 2450e6, "")?;

    let mut src = SoapySource::new(&d, 0)?;
    let sink = IqFileSink::<_, LittleEndian>::new("test.iq")?.block(src.connect()?);
    p.add(src);
    p.add(sink);

    p.run().join().unwrap();

    Ok(())
}
