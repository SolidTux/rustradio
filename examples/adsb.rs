use byteorder::LittleEndian;
use num_complex::Complex;
use rustradio::prelude::*;
//use soapysdr::{Device, Direction};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    let mut p = Program::default();

    //let d = Device::new("")?;
    //d.set_antenna(Direction::Rx, 0, "TX/RX")?;
    //d.set_bandwidth(Direction::Rx, 0, 4e6)?;
    //d.set_sample_rate(Direction::Rx, 0, 4e6)?;
    //d.set_gain(Direction::Rx, 0, 25.)?;
    //d.set_frequency(Direction::Rx, 0, 1090e6, "")?;

    //let mut src = SoapySource::new(&d, 0)?;
    let mut src = WavSource::<Complex<i16>>::new("adsb.wav")?;
    let mut convert = ComplexConverter::default().block(src.connect()?);
    let sink = IqFileSink::<Complex<f32>, LittleEndian>::new("test.iq")?.block(convert.connect()?);
    p.add(src);
    p.add(convert);
    p.add(sink);

    p.run().join().unwrap();

    Ok(())
}
