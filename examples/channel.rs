use log::Level;
use rustradio::prelude::*;
use std::{error::Error, sync::mpsc};

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    let (tx, rx) = mpsc::sync_channel(0);
    let mut p = Program::default();
    p.add(LogSink::new(Level::Info).block(rx));
    let handle = p.run();
    for i in 0..10 {
        tx.send(i).unwrap();
    }
    drop(tx);
    handle.join().unwrap();
    Ok(())
}
