use log::Level;
use rustradio::prelude::*;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    let mut p = Program::default();
    let mut src = ElementSource::new(1).block();
    p.add(LogSink::new(Level::Info).block(src.connect()?));
    p.add(src);
    p.run().join().unwrap();
    Ok(())
}
