use log::Level;
use rustradio::prelude::*;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    let mut p = Program::default();
    let mut src = (0..10).block();
    let sink = LogSink::new(Level::Info).block(src.connect()?);
    p.add(src);
    p.add(sink);
    p.run().join().unwrap();

    Ok(())
}
