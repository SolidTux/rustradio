use super::*;

use num_complex::Complex;
use num_traits::Num;
use std::marker::PhantomData;

#[derive(Default)]
pub struct Converter<F, T> {
    f: PhantomData<F>,
    t: PhantomData<T>,
}

impl<F, T> Modulation for Converter<F, T>
where
    F: Into<T>,
{
    type Input = F;
    type Output = T;
    type Error = &'static str;

    fn handle(&mut self, sample: Self::Input) -> Result<Self::Output, Self::Error> {
        Ok(sample.into())
    }
}

#[derive(Default)]
pub struct ComplexConverter<F, T> {
    f: PhantomData<F>,
    t: PhantomData<T>,
}

impl<F, T> Modulation for ComplexConverter<F, T>
where
    T: Num + Clone,
    F: Into<T>,
{
    type Input = Complex<F>;
    type Output = Complex<T>;
    type Error = &'static str;

    fn handle(&mut self, sample: Self::Input) -> Result<Self::Output, Self::Error> {
        Ok(Complex::new(sample.re.into(), sample.im.into()))
    }
}
