mod general;

pub use general::*;

use super::Block;
use crate::source::Splitter;

use crossbeam_channel::{Receiver, SendError, Sender};
use log::error;
use std::{
    fmt::Display,
    thread::{self, JoinHandle},
};

pub trait Modulation {
    type Input;
    type Output;
    type Error;

    fn handle(&mut self, sample: Self::Input) -> Result<Self::Output, Self::Error>;
    fn activate(&mut self) {}
}

pub trait IntoModulationBlock<S, T, U> {
    fn block(self, input: Receiver<T>) -> ModulationBlock<S, T, U>;
}

impl<S, T, U> IntoModulationBlock<S, T, U> for S
where
    T: Send + 'static,
    S: Modulation<Input = T, Output = U> + Send + 'static,
    U: Send + 'static + Clone,
    S::Error: Display,
{
    fn block(self, input: Receiver<T>) -> ModulationBlock<S, T, U> {
        ModulationBlock::new(self, input)
    }
}

pub struct ModulationBlock<S, T, U> {
    modulate: S,
    input: Receiver<T>,
    tx: Sender<U>,
    output: Splitter<U>,
}

impl<S, T, U> ModulationBlock<S, T, U>
where
    T: Send + 'static,
    S: Modulation<Input = T, Output = U> + Send + 'static,
    U: Send + 'static + Clone,
    S::Error: Display,
{
    pub fn new(modulate: S, input: Receiver<T>) -> Self {
        let (tx, rx) = crossbeam_channel::bounded(100_000);
        ModulationBlock {
            modulate,
            input,
            tx,
            output: Splitter::new(rx),
        }
    }

    pub fn connect(&mut self) -> Result<Receiver<U>, SendError<Sender<U>>> {
        self.output.connect()
    }
}

impl<S, T, U> Block for ModulationBlock<S, T, U>
where
    T: Send + 'static + Clone,
    S: Modulation<Input = T, Output = U> + Send + 'static,
    U: Send + 'static + Clone,
    S::Error: Display,
{
    fn run(mut self: Box<Self>) -> JoinHandle<()> {
        thread::spawn(move || {
            while let Ok(s) = self.input.recv() {
                match self.modulate.handle(s) {
                    Ok(t) => {
                        if let Err(e) = self.tx.send(t.clone()) {
                            error!("{}", e);
                            break;
                        }
                    }
                    Err(e) => error!("{}", e),
                }
            }
        })
    }

    fn activate(&mut self) {
        self.modulate.activate();
    }
}
