pub mod modulate;
pub mod sink;
pub mod source;

use std::thread::JoinHandle;

pub trait Block {
    fn run(self: Box<Self>) -> JoinHandle<()>;
    fn activate(&mut self) {}
}

pub mod prelude {
    pub use crate::{modulate::*, sink::*, source::*, *};
}

#[derive(Default)]
pub struct Program {
    blocks: Vec<Box<dyn Block>>,
}

impl Program {
    pub fn add<B: Block + 'static>(&mut self, b: B) {
        self.blocks.push(Box::new(b));
    }

    pub fn run(mut self) -> ProgramHandle {
        let mut handles = Vec::new();
        for b in &mut self.blocks {
            b.activate();
        }
        for b in self.blocks {
            handles.push(b.run());
        }
        ProgramHandle { handles }
    }
}

pub struct ProgramHandle {
    handles: Vec<JoinHandle<()>>,
}

impl ProgramHandle {
    pub fn join(self) -> ::std::thread::Result<()> {
        for h in self.handles {
            h.join()?;
        }
        Ok(())
    }
}
