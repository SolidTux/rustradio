use super::*;

use byteorder::{ByteOrder, WriteBytesExt};
use num_complex::Complex;
use std::{fs::File, io::Error, marker::PhantomData, path::Path};

pub struct IqFileSink<T, B> {
    f: File,
    t: PhantomData<T>,
    b: PhantomData<B>,
}

impl<T, B> IqFileSink<T, B> {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let f = File::create(path)?;
        Ok(IqFileSink {
            f,
            t: PhantomData,
            b: PhantomData,
        })
    }
}

impl<B> Sink for IqFileSink<Complex<f32>, B>
where
    B: ByteOrder,
{
    type Sample = Complex<f32>;
    type Error = Error;

    fn handle(&mut self, sample: Self::Sample) -> Result<(), Self::Error> {
        self.f.write_f32::<B>(sample.re)?;
        self.f.write_f32::<B>(sample.im)?;
        Ok(())
    }
}

impl<B> Sink for IqFileSink<Complex<i16>, B>
where
    B: ByteOrder,
{
    type Sample = Complex<i16>;
    type Error = Error;

    fn handle(&mut self, sample: Self::Sample) -> Result<(), Self::Error> {
        self.f.write_i16::<B>(sample.re)?;
        self.f.write_i16::<B>(sample.im)?;
        Ok(())
    }
}
