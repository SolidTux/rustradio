use super::*;
use log::{log, Level};
use std::marker::PhantomData;

pub struct LogSink<T> {
    level: Level,
    t: PhantomData<T>,
}

impl<T> LogSink<T> {
    pub fn new(level: Level) -> Self {
        LogSink {
            level,
            t: PhantomData,
        }
    }
}

impl<T> Sink for LogSink<T>
where
    T: Display + Send + 'static,
{
    type Sample = T;
    type Error = &'static str;

    fn handle(&mut self, sample: Self::Sample) -> Result<(), Self::Error> {
        log!(self.level, "{}", sample);
        Ok(())
    }
}
