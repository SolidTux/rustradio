mod general;
mod iq;

pub use general::*;
pub use iq::*;

use super::Block;

use crossbeam_channel::Receiver;
use log::error;
use std::{
    fmt::Display,
    thread::{self, JoinHandle},
};

pub trait Sink {
    type Sample;
    type Error;

    fn handle(&mut self, sample: Self::Sample) -> Result<(), Self::Error>;
    fn activate(&mut self) {}
}

pub trait IntoSinkBlock<S, T> {
    fn block(self, input: Receiver<T>) -> SinkBlock<S, T>;
}

impl<S, T> IntoSinkBlock<S, T> for S
where
    T: Send + 'static,
    S: Sink<Sample = T> + Send + 'static,
    S::Error: Display,
{
    fn block(self, input: Receiver<T>) -> SinkBlock<S, T> {
        SinkBlock::new(self, input)
    }
}

pub struct SinkBlock<S, T> {
    sink: S,
    input: Receiver<T>,
}

impl<S, T> SinkBlock<S, T>
where
    T: Send + 'static,
    S: Sink<Sample = T> + Send + 'static,
    S::Error: Display,
{
    pub fn new(sink: S, input: Receiver<T>) -> Self {
        SinkBlock { sink, input }
    }
}

impl<S, T> Block for SinkBlock<S, T>
where
    T: Send + 'static,
    S: Sink<Sample = T> + Send + 'static,
    S::Error: Display,
{
    fn run(mut self: Box<Self>) -> JoinHandle<()> {
        thread::spawn(move || {
            while let Ok(s) = self.input.recv() {
                if let Err(e) = self.sink.handle(s) {
                    error!("{}", e);
                }
            }
        })
    }

    fn activate(&mut self) {
        self.sink.activate();
    }
}
