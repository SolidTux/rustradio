mod general;
#[cfg(feature = "soapy")]
mod soapy;

pub use general::*;
pub use soapy::*;

use super::Block;

use crossbeam_channel::{self, Receiver, SendError, Sender, TryRecvError};
use log::error;
use std::thread::{self, JoinHandle};

pub trait Source {
    type Sample;

    fn generate(&mut self) -> Option<Self::Sample>;
    fn activate(&mut self) {}
}

impl<I> Source for I
where
    I: Iterator,
{
    type Sample = I::Item;

    fn generate(&mut self) -> Option<Self::Sample> {
        self.next()
    }
}

pub trait IntoSourceBlock<T, S> {
    fn block(self) -> SourceBlock<T, S>;
}

impl<T, S> IntoSourceBlock<T, S> for S
where
    T: Send + Clone + 'static,
    S: Source<Sample = T> + Send + 'static,
{
    fn block(self) -> SourceBlock<T, S> {
        SourceBlock::new(self)
    }
}

pub struct SourceBlock<T, S> {
    tx: Sender<T>,
    source: S,
    output: Splitter<T>,
}

impl<T, S> SourceBlock<T, S>
where
    T: Send + Clone + 'static,
    S: Source<Sample = T> + Send + 'static,
{
    pub fn new(source: S) -> SourceBlock<T, S> {
        let (tx, rx) = crossbeam_channel::bounded(100_000);
        let output = Splitter::new(rx);
        SourceBlock { tx, source, output }
    }

    pub fn connect(&mut self) -> Result<Receiver<T>, SendError<Sender<T>>> {
        self.output.connect()
    }
}

impl<T, S> Block for SourceBlock<T, S>
where
    T: Send + Clone + 'static,
    S: Source<Sample = T> + Send + 'static,
{
    fn run(mut self: Box<Self>) -> JoinHandle<()> {
        thread::spawn(move || {
            while let Some(s) = self.source.generate() {
                if let Err(e) = self.tx.send(s) {
                    error!("{}", e);
                    break;
                }
            }
        })
    }

    fn activate(&mut self) {
        self.source.activate();
    }
}

pub struct Splitter<T> {
    tx: Sender<Sender<T>>,
}

impl<T> Splitter<T>
where
    T: Send + Clone + 'static,
{
    pub fn new(input: Receiver<T>) -> Self {
        let (tx, rx): (Sender<Sender<T>>, Receiver<Sender<T>>) = crossbeam_channel::unbounded();
        thread::spawn(move || {
            let mut outputs = Vec::new();
            while let Ok(s) = input.recv() {
                match rx.try_recv() {
                    Ok(o) => outputs.push(o),
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => break,
                }

                for o in &outputs {
                    if let Err(e) = o.send(s.clone()) {
                        error!("{}", e);
                        break;
                    }
                }
            }
        });

        Splitter { tx }
    }

    pub fn connect(&mut self) -> Result<Receiver<T>, SendError<Sender<T>>> {
        let (tx, rx) = crossbeam_channel::bounded(100_000);
        self.tx.send(tx)?;
        Ok(rx)
    }
}
