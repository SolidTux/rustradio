use super::*;

use hound::{Sample, WavIntoSamples, WavReader};
use log::error;
use num_complex::Complex;
use num_traits::Num;
use std::{fs::File, io::BufReader, path::Path};

pub struct ElementSource<T> {
    e: T,
}

impl<T> ElementSource<T> {
    pub fn new(e: T) -> Self {
        ElementSource { e }
    }
}

impl<T> Source for ElementSource<T>
where
    T: Send + Clone + 'static,
{
    type Sample = T;

    fn generate(&mut self) -> Option<Self::Sample> {
        Some(self.e.clone())
    }
}

pub struct WavSource<T> {
    reader: WavReader<BufReader<File>>,
    tx: Sender<T>,
    output: Splitter<T>,
}

impl<T> WavSource<T>
where
    T: Clone + Send + 'static,
{
    pub fn new<P: AsRef<Path>>(filename: P) -> Result<Self, hound::Error> {
        let (tx, rx) = crossbeam_channel::bounded(100_000);
        Ok(Self {
            reader: WavReader::open(filename)?,
            tx,
            output: Splitter::new(rx),
        })
    }

    pub fn connect(&mut self) -> Result<Receiver<T>, SendError<Sender<T>>> {
        self.output.connect()
    }
}

impl<T> Block for WavSource<Complex<T>>
where
    T: Clone + Send + 'static + Sample + Num,
{
    fn run(self: Box<Self>) -> JoinHandle<()> {
        thread::spawn(move || {
            let mut samples: WavIntoSamples<_, T> = self.reader.into_samples();
            let mut last = None;
            loop {
                match samples.next() {
                    Some(Ok(s)) => match last {
                        None => last = Some(s.clone()),
                        Some(l) => {
                            if let Err(e) = self.tx.send(Complex::new(l, s.clone())) {
                                error!("{}", e);
                                break;
                            }
                            last = None;
                        }
                    },
                    Some(Err(e)) => {
                        error!("{}", e);
                        break;
                    }
                    None => break,
                }
            }
        })
    }
}
