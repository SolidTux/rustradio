use super::*;
use crate::Block;

use crossbeam_channel::{SendError, Sender};
use log::error;
use num_traits::Zero;
use soapysdr::{Device, Error, RxStream, StreamSample};
use std::thread::JoinHandle;

pub struct SoapySource<T>
where
    T: StreamSample,
{
    stream: RxStream<T>,
    tx: Sender<T>,
    output: Splitter<T>,
}

impl<T> SoapySource<T>
where
    T: StreamSample + Zero + Clone + Send + 'static,
{
    // TODO multiple channels
    pub fn new(device: &Device, channel: usize) -> Result<Self, Error> {
        let stream = device.rx_stream(&[channel])?;
        let (tx, rx) = crossbeam_channel::bounded(100_000);
        Ok(SoapySource {
            stream,
            tx,
            output: Splitter::new(rx),
        })
    }

    pub fn connect(&mut self) -> Result<Receiver<T>, SendError<Sender<T>>> {
        self.output.connect()
    }
}

impl<T> Block for SoapySource<T>
where
    T: StreamSample + Zero + Clone + Send + 'static + std::fmt::Display,
{
    fn run(mut self: Box<Self>) -> JoinHandle<()> {
        thread::spawn(move || {
            let mut buffer = vec![T::zero(); self.stream.mtu().unwrap_or(1000)];
            if let Err(e) = self.stream.activate(None) {
                error!("{}", e);
                return;
            }
            loop {
                match self.stream.read(&[&mut buffer], 10_000_000) {
                    Ok(n) => {
                        for i in 0..n {
                            if let Err(e) = self.tx.send(buffer[i].clone()) {
                                error!("{}", e);
                                return;
                            }
                        }
                    }
                    Err(e) => {
                        error!("{}", e);
                        return;
                    }
                }
            }
        })
    }
}
